<div class="row">
  <div class="col-md-8">
    <h1 class="text-center">LISTADO DE LOCACIONES</h1>
  </div>
  <div class="col-md-4">
    <a href="<?php echo site_url();?>/locaciones/nuevo"><i class="glyphicon glyphicon-plus"></i> Nueva Locación</a>
  </div>
</div>
<hr>
<?php if ($locaciones): ?>
  <table class="table table-bordered table-hover">
    <thead class="thead-dark">
      <tr>
        <th>ID</th>
        <th>Nombre</th>
        <th>Dirección</th>
        <th>Capacidad</th>
        <th>Horarios</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($locaciones as $filatemporal): ?>
        <tr>
          <td>
            <?php echo $filatemporal->id_loc?>
          </td>
          <td>
            <?php echo $filatemporal->nombre_loc?>
          </td>
          <td>
            <?php echo $filatemporal->direccion_loc?>
          </td>
          <td>
            <?php echo $filatemporal->capacidad_loc?>
          </td>
          <td>
            <?php echo $filatemporal->horario_loc?>
          </td>
          <td class="text-center">
            <a href="#" title="Editar Locacion" style="color:green">
              <i class="glyphicon glyphicon-pencil"></i>
            </a>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <a href="<?php echo site_url();?>/locaciones/eliminar/<?php echo $filatemporal->id_loc?>" title="Eliminar Locacion" style="color:red">
              <i class="glyphicon glyphicon-trash"></i>
            </a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php
  // print_r($instructores); un print_r para imprimir todo el valor del array y comprobar que si pasó la información
 ?>
<?php else: ?>
<h1>No hay datos</h1>
<?php endif; ?>
