<h1>NUEVA LOCACIÓN</h1>
<!-- Aqui se pone que va a abrir con el action -->
<!-- En este caso el actin abre la función guardar desde instructores -->
<!-- Además se debe poner el botón del formuario del tipo submit para que envie la información -->
<form class=""
action="<?php echo site_url();?>/Locaciones/guardar"
method="post">
<!-- Aqui se define el tipo de método para el input, este es de tipo post -->
    <div class="row">
      <div class="col-md-6">
          <label for="">Nombre:</label>
          <br>
          <input type="text"
          placeholder="Ingrese nombre"
          class="form-control"
          name="nombre_loc" value=""
          id="nombre_loc">
      </div>
      <div class="col-md-6">
          <label for="">Dirección:</label>
          <br>
          <input type="text"
          placeholder="Ingrese la direccion"
          class="form-control"
          name="direccion_loc" value=""
          id="nombre_loc">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-6">
          <label for="">Horario:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el horario"
          class="form-control"
          name="horario_loc" value=""
          id="nombre_loc">
      </div>
      <div class="col-md-6">
        <label for="">Capacidad:</label>
        <br>
        <input type="text"
        placeholder="Ingrese la capacidad"
        class="form-control"
        name="capacidad_loc" value=""
        id="capacidad_ins">
      </div>
    </div>

    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url();?>/locaciones/index"class="btn btn-danger">Cancelar</a>
        </div>
    </div>
</form>
