<?php if ($instructores): ?>
  <table class="table table-bordered table-hover">
    <thead class="thead-dark">
      <tr>
        <th>ID</th>
        <th>Cedula</th>
        <th>Primer Apellido</th>
        <th>Segundo Apellido</th>
        <th>Nombres</th>
        <th>Titulo</th>
        <th>Telefono</th>
        <th>Dirección</th>
        <th>Acciones</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($instructores as $filatemporal): ?>
        <tr>
          <td>
            <?php echo $filatemporal->id_ins?>
          </td>
          <td>
            <?php echo $filatemporal->cedula_ins?>
          </td>
          <td>
            <?php echo $filatemporal->primer_apellido_ins?>
          </td>
          <td>
            <?php echo $filatemporal->segundo_apellido_ins?>
          </td>
          <td>
            <?php echo $filatemporal->nombres_ins?>
          </td>
          <td>
            <?php echo $filatemporal->titulo_ins?>
          </td>
          <td>
            <?php echo $filatemporal->telefono_ins?>
          </td>
          <td>
            <?php echo $filatemporal->direccion_ins?>
          </td>
          <td class="text-center">
            <a href="<?php echo site_url();?>/instructores/editar/<?php echo $filatemporal->id_ins?>" title="Editar Instructor" style="color:green">
              <i class="mdi mdi-pencil"></i> Editar
            </a>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <a href="<?php echo site_url();?>/instructores/eliminar/<?php echo $filatemporal->id_ins?>" title="Eliminar Instructor" style="color:red">
              <i class="mdi mdi-close"></i> Eliminar
            </a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php
  // print_r($instructores); un print_r para imprimir todo el valor del array y comprobar que si pasó la información
 ?>
<?php else: ?>
<h1>No hay datos</h1>
<?php endif; ?>

<script>
$(document).ready(function() {
    toastr.success('<?php echo $mensaje; ?>');
});
</script>
