<form class=""
id="form_editar"
action="<?php echo site_url('Instructores/procesarActualizacion');?>"
method="post">
<!-- Aqui se define el tipo de método para el input, este es de tipo post -->
<h1>EDITAR INSTRUCTOR</h1>
    <div class="row">
      <div class="col-md-4">
        <input type="text" name="id_ins" id="id_ins" value="<?php echo $instructorEditar->id_ins;?>">
          <label for="">Cédula: </label>
          <br>
          <input type="number"
          placeholder="Ingrese la cédula"
          class="form-control"
          name="cedula_ins" value="<?php echo $instructorEditar->cedula_ins;?>"
          required
          id="cedula_ins" <span class="obligatorio"></span>
      </div>
      <div class="col-md-4">
          <label for="">Primer Apellido:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el primer apellido"
          class="form-control"
          name="primer_apellido_ins" value="<?php echo $instructorEditar->primer_apellido_ins;?>"
          id="primer_apellido_ins">
      </div>
      <div class="col-md-4">
        <label for="">Segundo Apellido:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el segundo apellido"
        class="form-control"
        name="segundo_apellido_ins" value="<?php echo $instructorEditar->segundo_apellido_ins;?>"
        id="segundo_apellido_ins">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Nombre:</label>
          <br>
          <input type="text"
          placeholder="Ingrese los nombres"
          class="form-control"
          name="nombres_ins" value="<?php echo $instructorEditar->nombres_ins;?>"
          id="nombres_ins">
      </div>
      <div class="col-md-4">
          <label for="">Título:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el titulo"
          class="form-control"
          name="titulo_ins" value="<?php echo $instructorEditar->titulo_ins;?>"
          id="titulo_ins">
      </div>
      <div class="col-md-4">
        <label for="">Teléfono:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el telefono"
        class="form-control"
        name="telefono_ins" value="<?php echo $instructorEditar->telefono_ins;?>"
        id="telefono_ins">
      </div>
    </div>

    <br>
    <div class="row">
      <div class="col-md-12">
          <label for="">Dirección:</label>
          <br>
          <input type="text"
          placeholder="Ingrese la direccion"
          class="form-control"
          name="direccion_ins" value="<?php echo $instructorEditar->direccion_ins;?>"
          id="direccion_ins">
      </div>
    </div>

    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url();?>/instructores/index"class="btn btn-danger">Cancelar</a>
        </div>
    </div>

    <script type="text/javascript">

    $("#form_editar").validate({
      rules:{
        cedula_ins:{
          required:true,
          minlength:10,
          maxlength:10
        },
          primer_apellido_ins:{
            required:true,
            lettersonly: true,
            minlength:3,
            maxlength:250,
          }
      },
      messages:{
        cedula_ins:{
          required:"Número de cédula faltante",
          minlength:"Cedula Incorrecta, ingrese 10 digitos",
          maxlength:"Cedula Incorrecta, ingrese 10 digitos"
        },
        primer_apellido_ins:{
          required:"Por favor ingrese el primer apellido",
          lettersonly: "Solo letras",
          minlength:"Ingrese un apellido válido",
          maxlength:"Ingrese un apellido válido"
        }
      }
    });
    </script>

</form>
