<form class=""
id="form_nuevo"
action="<?php echo site_url();?>/Instructores/guardar"
method="post">
<!-- Aqui se define el tipo de método para el input, este es de tipo post -->
<h1>NUEVO INSTRUCTOR</h1>
    <div class="row">
      <div class="col-md-4">
          <label for="">Cédula:</label>
          <br>
          <input type="number"
          placeholder="Ingrese la cédula"
          class="form-control"
          name="cedula_ins" value=""
          required
          id="cedula_ins"   <span class="obligatorio"></span>
      </div>
      <div class="col-md-4">
          <label for="">Primer Apellido:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el primer apellido"
          class="form-control"
          name="primer_apellido_ins" value=""
          id="primer_apellido_ins">
      </div>
      <div class="col-md-4">
        <label for="">Segundo Apellido:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el segundo apellido"
        class="form-control"
        name="segundo_apellido_ins" value=""
        id="segundo_apellido_ins">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Nombre:</label>
          <br>
          <input type="text"
          placeholder="Ingrese los nombres"
          class="form-control"
          name="nombres_ins" value=""
          id="nombres_ins">
      </div>
      <div class="col-md-4">
          <label for="">Título:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el titulo"
          class="form-control"
          name="titulo_ins" value=""
          id="titulo_ins">
      </div>
      <div class="col-md-4">
        <label for="">Teléfono:</label>
        <br>
        <input type="number"
        placeholder="Ingrese el telefono"
        class="form-control"
        name="telefono_ins" value=""
        id="telefono_ins">
      </div>
    </div>

    <br>
    <div class="row">
      <div class="col-md-12">
          <label for="">Dirección:</label>
          <br>
          <input type="text"
          placeholder="Ingrese la direccion"
          class="form-control"
          name="direccion_ins" value=""
          id="direccion_ins">
      </div>
    </div>

    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url();?>/instructores/index"class="btn btn-danger">Cancelar</a>
        </div>
    </div>

    <script type="text/javascript">

    $("#form_nuevo").validate({
      rules:{
          cedula_ins:{
          required:true,
          minlength:10,
          maxlength:10
        },
          primer_apellido_ins:{
            required:true,
            lettersonly: true,
            minlength:3,
            maxlength:250,
          },
          segundo_apellido_ins:{
            required:true,
            lettersonly: true,
            minlength:3,
            maxlength:250,
          },
          nombres_ins:{
            required:true,
            lettersonly: true,
            minlength:3,
            maxlength:250,
          },
          titulo_ins:{
            required:true,
            lettersonly: true,
            minlength:3,
            maxlength:250,
          },
          telefono_ins:{
            required:true,
            minlength:7,
            maxlength:10
          },
          direccion_ins:{
            required:true,
            minlength:3,
            maxlength:250,
          },
      },
      messages:{
        cedula_ins:{
          required:"Número de cédula faltante",
          minlength:"Cedula Incorrecta, ingrese 10 digitos",
          maxlength:"Cedula Incorrecta, ingrese 10 digitos"
        },
        primer_apellido_ins:{
          required:"Por favor ingrese el primer apellido",
          lettersonly: "Solo letras",
          minlength:"Ingrese un apellido válido",
          maxlength:"Ingrese un apellido válido"
        },
        segundo_apellido_ins:{
          required:"Por favor ingrese el segundo apellido",
          lettersonly: "Solo letras",
          minlength:"Ingrese un apellido válido",
          maxlength:"Ingrese un apellido válido"
        },
        nombres_ins:{
          required:"Por favor ingrese los nombres",
          lettersonly: "Solo letras",
          minlength:"Ingrese un apellido válido",
          maxlength:"Ingrese un apellido válido"
        },
        titulo_ins:{
          required:"Por favor ingrese el título",
          lettersonly: "Solo letras",
          minlength:"Ingrese un título válido",
          maxlength:"Ingrese un título válido"
        },
        direccion_ins:{
          required:"Por favor ingrese una dirección",
          minlength:"Ingrese una dirección válida",
          maxlength:"Ingrese una dirección válida"
        },
        telefono_ins:{
          required:"Número de teléfono faltante",
          minlength:"Teléfono Incorrecto, ingrese entre 7 y 10 digitos",
          maxlength:"Teléfono Incorrecto, ingrese entre 7 y 10 digitos"
        }
      }
    });
    </script>

</form>
