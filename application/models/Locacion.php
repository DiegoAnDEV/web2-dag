<?php
  class Locacion extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un instructor en MYSQL
    function insertar($datosNuevaLocacion){
        return $this->db->insert("locaciones",$datosNuevaLocacion);
    }
    function obtenerTodos(){
      $listadoLocaciones=$this->db->get('locaciones');
      if ($listadoLocaciones->num_rows()>0) {
        return $listadoLocaciones->result();
      } else {
        return false;
      }
    }
    function borrar($id_loc){
      $this->db->where('id_loc',$id_loc);
      return $this->db->delete('locaciones');
    }
  }//Cierre de la clase
 ?>
