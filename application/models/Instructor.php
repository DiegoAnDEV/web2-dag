<?php
  class Instructor extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un instructor en MYSQL
    function insertar($datosNuevoInstructor){
        return $this->db->insert("instructor",$datosNuevoInstructor);
    }
    function obtenerTodos(){
      $listadoInstructores=$this->db->get('instructor');
      if ($listadoInstructores->num_rows()>0) {
        return $listadoInstructores->result();
      } else {
        return false;
      }
    }
    function borrar($id_ins){
      $this->db->where('id_ins',$id_ins);
      return $this->db->delete('instructor');
    }
    // Función para consultar where
    function obtenerID($id_ins){
      $this->db->where('id_ins',$id_ins);
      $instructor=$this->db->get('instructor');
      if ($instructor->num_rows()>0) {
        return $instructor->row();
      }else{
        return false;
      }
    }
    //funcion para actualizar un instructor
    function actualizar($id_ins,$datosEditados)
    {
      $this->db->where('id_ins',$id_ins);
      return $this->db->update('instructor',$datosEditados);
    }
  }//Cierre de la clase
 ?>
