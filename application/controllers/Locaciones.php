<?php
  class Locaciones extends CI_Controller
  {
    // Constructor
    function __construct()
    {
      parent::__construct();
      $this->load->model('Locacion');
    }
    public function index(){
      $dataLoc['locaciones']=$this->Locacion->obtenerTodos();
      $this->load->view('header');
  		$this->load->view('locaciones/index',$dataLoc);
  		$this->load->view('footer');
    }
    public function nuevo(){
      $this->load->view('header');
  		$this->load->view('locaciones/nuevo');
  		$this->load->view('footer');
    }
    public function guardar(){
      $datosNuevaLocacion = array(
        'id_loc' =>$this->input->post('id_loc'),
        'nombre_loc' =>$this->input->post('nombre_loc'),
        'direccion_loc' =>$this->input->post('direccion_loc'),
        'capacidad_loc' =>$this->input->post('capacidad_loc'),
        'horario_loc' =>$this->input->post('horario_loc'),
      );

      if ($this->Locacion->insertar($datosNuevaLocacion)) {
        redirect('locaciones/index');
      } else {
        echo "<h1>ERROR AL INSERTAR</h1>";
      }
    }
    // Funcion eliminar locacion
    public function eliminar($id_loc){
      echo $id_loc;
      if ($this->Locacion->borrar($id_loc)) {
        redirect('locaciones/index');
      } else {
        echo "<h1>ERROR AL BORRAR</h1>";
      }
    }
  } // Cierre de la clase
?>
